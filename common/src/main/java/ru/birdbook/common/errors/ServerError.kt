package ru.greenminds.runner.common.errors

class ServerError constructor(val serverErrorMessage: String) : Error()