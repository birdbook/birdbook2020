package ru.greenminds.runner.common.errors

class UnexpectedError constructor(val throwable: Throwable) : Error()