package ru.birdbook.coreui.error

import android.text.TextUtils
import androidx.appcompat.app.AppCompatActivity
import javax.inject.Inject

class ErrorView @Inject constructor(
    private val activity: AppCompatActivity
) {
    fun showNetworkError() {
//        NetworkErrorDialog().show(activity.supportFragmentManager, NetworkErrorDialog::class.java.name)
    }

    fun showErrorMessage(message: String?) {
        if (message == null || TextUtils.isEmpty(message)) {
            return
        }
//        MessageDialog.showWithText(activity.supportFragmentManager, message)
    }

    fun showUnexpectedError() {
//        UnexpectedErrorDialog().show(activity.supportFragmentManager, UnexpectedErrorDialog::class.java.name)
    }

}