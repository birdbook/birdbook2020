package ru.birdbook.coreui.error

import android.content.res.Resources
import androidx.annotation.CallSuper
import androidx.lifecycle.MutableLiveData
import ru.birdbook.coreui.R

open class DefaultErrorCallback(
    private val messageLiveData: MutableLiveData<String>,
    private val resources: Resources
) : ErrorCallbackImpl() {

    @CallSuper
    override fun onUnauthorizedError() {
        super.onUnauthorizedError()
        messageLiveData.value = resources.getString(R.string.unauthorized_error)
    }

    @CallSuper
    override fun onServerError(message: String) {
        super.onServerError(message)
        messageLiveData.value = message
    }

    @CallSuper
    override fun onUnexpectedError(throwable: Throwable) {
        super.onUnexpectedError(throwable)
        messageLiveData.value = resources.getString(R.string.unexpected_error)
    }

    @CallSuper
    override fun onNetworkError() {
        super.onNetworkError()
        messageLiveData.value = resources.getString(R.string.network_error)
    }
}