package ru.birdbook.coreui.error

import io.reactivex.exceptions.CompositeException
import io.reactivex.functions.Consumer
import ru.greenminds.runner.common.errors.NetworkError
import ru.greenminds.runner.common.errors.ServerError
import ru.greenminds.runner.common.errors.UnauthorizedError
import timber.log.Timber

object RxError {

    fun error(view: ErrorCallback): Consumer<Throwable> = Consumer { e ->
        Timber.w(e, "from RxError.error")
        when (e) {
            is CompositeException -> parseCompositeException(view, e)
            is NetworkError -> view.onNetworkError()
            is ServerError -> view.onServerError(e.serverErrorMessage)
            is UnauthorizedError -> view.onUnauthorizedError()
            else -> view.onUnexpectedError(e)
        }
    }

    private fun parseCompositeException(
        view: ErrorCallback,
        compositeException: CompositeException
    ) {
        for (exception in compositeException.exceptions) {
            when (exception) {
                is NetworkError -> view.onNetworkError()
                is ServerError -> view.onServerError(exception.serverErrorMessage)
                is UnauthorizedError -> view.onUnauthorizedError()
                else -> view.onUnexpectedError(compositeException)
            }
        }
    }
}