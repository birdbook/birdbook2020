package ru.birdbook.coreui.error

import androidx.annotation.CallSuper
import ru.greenminds.runner.common.errors.UnexpectedError
import timber.log.Timber

interface ErrorCallback {

    fun onNetworkError()

    fun onUnauthorizedError()

    fun onServerError(message: String)

    fun onUnexpectedError(throwable: Throwable)
}

open class ErrorCallbackImpl : ErrorCallback {

    @CallSuper
    override fun onUnauthorizedError() {
        Timber.e("onUnauthorizedError")
    }

    @CallSuper
    override fun onServerError(message: String) {
        Timber.e("Server error: %s", message)
    }

    @CallSuper
    override fun onUnexpectedError(throwable: Throwable) {
        if (throwable is UnexpectedError) {
            Timber.e(throwable.throwable)
        } else {
            Timber.e(throwable)
        }
    }

    @CallSuper
    override fun onNetworkError() {
        Timber.e("onNetworkError")
    }
}
