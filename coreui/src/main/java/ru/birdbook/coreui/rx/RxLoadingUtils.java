package ru.birdbook.coreui.rx;

import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import io.reactivex.CompletableTransformer;
import io.reactivex.FlowableTransformer;
import io.reactivex.MaybeTransformer;
import io.reactivex.ObservableTransformer;
import io.reactivex.SingleTransformer;

public class RxLoadingUtils {

    @MainThread
    @NonNull
    public static <T> MaybeTransformer<T, T> maybeLoading(@NonNull MutableLiveData<Boolean> loadingLiveData) {
        return observable -> observable
                .doOnSubscribe(disposable -> loadingLiveData.setValue(true))
                .doOnTerminate(() -> loadingLiveData.setValue(false))
                .doOnDispose(() -> loadingLiveData.setValue(false));
    }

    @MainThread
    @NonNull
    public static <T> MaybeTransformer<T, T> maybePostLoading(@NonNull MutableLiveData<Boolean> loadingLiveData) {
        return observable -> observable
                .doOnSubscribe(disposable -> loadingLiveData.postValue(true))
                .doOnTerminate(() -> loadingLiveData.postValue(false))
                .doOnDispose(() -> loadingLiveData.postValue(false));
    }

    @MainThread
    @NonNull
    public static <T> MaybeTransformer<T, T> maybeLoadingSilently(@NonNull MutableLiveData<Boolean> loadingLiveData, boolean isSilently) {
        return source -> isSilently
                ? source
                : source.compose(maybeLoading(loadingLiveData));
    }

    @MainThread
    @NonNull
    public static <T> SingleTransformer<T, T> singleLoading(@NonNull MutableLiveData<Boolean> loadingLiveData) {
        return observable -> observable
                .doOnSubscribe(disposable -> loadingLiveData.setValue(true))
                .doOnTerminate(() -> loadingLiveData.setValue(false))
                .doOnDispose(() -> loadingLiveData.setValue(false));
    }

    @MainThread
    @NonNull
    public static <T> SingleTransformer<T, T> singlePostLoading(@NonNull MutableLiveData<Boolean> loadingLiveData) {
        return observable -> observable
                .doOnSubscribe(disposable -> loadingLiveData.postValue(true))
                .doOnTerminate(() -> loadingLiveData.postValue(false))
                .doOnDispose(() -> loadingLiveData.postValue(false));
    }

    @MainThread
    @NonNull
    public static <T> ObservableTransformer<T, T> observableLoading(@NonNull MutableLiveData<Boolean> loadingLiveData) {
        return observable -> observable
                .doOnSubscribe(disposable -> loadingLiveData.setValue(true))
                .doOnTerminate(() -> loadingLiveData.setValue(false))
                .doOnDispose(() -> loadingLiveData.setValue(false));
    }

    @MainThread
    @NonNull
    public static <T> FlowableTransformer<T, T> flowableLoading(@NonNull MutableLiveData<Boolean> loadingLiveData) {
        return flowable -> flowable
                .doOnSubscribe(disposable -> loadingLiveData.setValue(true))
                .doOnNext(value -> loadingLiveData.postValue(false))
                .doOnTerminate(() -> loadingLiveData.setValue(false))
                .doOnCancel(() -> loadingLiveData.setValue(false));
    }

    @MainThread
    @NonNull
    public static <T> FlowableTransformer<T, T> flowablePostLoading(@NonNull MutableLiveData<Boolean> loadingLiveData) {
        return flowable -> flowable
                .doOnSubscribe(disposable -> loadingLiveData.postValue(true))
                .doOnNext(value -> loadingLiveData.postValue(false))
                .doOnTerminate(() -> loadingLiveData.postValue(false))
                .doOnCancel(() -> loadingLiveData.postValue(false));
    }

    @MainThread
    @NonNull
    public static CompletableTransformer completableLoading(@NonNull MutableLiveData<Boolean> loadingLiveData) {
        return observable -> observable
                .doOnSubscribe(disposable -> loadingLiveData.setValue(true))
                .doOnTerminate(() -> loadingLiveData.setValue(false))
                .doOnDispose(() -> loadingLiveData.setValue(false));
    }

    @MainThread
    @NonNull
    public static CompletableTransformer completablePostLoading(@NonNull MutableLiveData<Boolean> loadingLiveData) {
        return observable -> observable
                .doOnSubscribe(disposable -> loadingLiveData.postValue(true))
                .doOnTerminate(() -> loadingLiveData.postValue(false))
                .doOnDispose(() -> loadingLiveData.postValue(false));
    }

    @MainThread
    @NonNull
    public static CompletableTransformer completableLoadingSilently(@NonNull MutableLiveData<Boolean> loadingLiveData, boolean isSilently) {
        return completable -> isSilently
                ? completable
                : completable.compose(completableLoading(loadingLiveData));
    }

    @MainThread
    @NonNull
    public static <T> SingleTransformer<T, T> singleLoadingSilently(@NonNull MutableLiveData<Boolean> loadingLiveData, boolean isSilently) {
        return completable -> isSilently
                ? completable
                : completable.compose(singleLoading(loadingLiveData));
    }
}
