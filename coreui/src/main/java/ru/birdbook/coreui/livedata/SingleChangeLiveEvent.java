package ru.birdbook.coreui.livedata;

import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

/**
 * @author Nikita Simonov on 09.07.2018.
 */
public class SingleChangeLiveEvent<T> extends SingleLiveEvent<T> {

    @MainThread
    @Override
    public void observe(@NonNull LifecycleOwner owner, @NonNull Observer<? super T> observer) {
        super.observe(owner, observer);
    }

    @MainThread
    @Override
    public void setValue(@Nullable T t) {
        if (t == null && getValue() == null
                || t != null && getValue() != null && t.equals(getValue())) {
            return;
        }
        super.setValue(t);
    }
}
