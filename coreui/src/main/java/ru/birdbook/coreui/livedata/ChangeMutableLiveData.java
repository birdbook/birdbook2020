package ru.birdbook.coreui.livedata;

import androidx.annotation.MainThread;
import androidx.annotation.Nullable;
import androidx.lifecycle.MutableLiveData;

/**
 * LiveData that calls observe just in case it's value was changed
 */
public class ChangeMutableLiveData<T> extends MutableLiveData<T> {

    @MainThread
    @Override
    public void setValue(@Nullable T t) {
        if (t == null && getValue() == null
                || t != null && getValue() != null && t.equals(getValue())) {
            return;
        }
        super.setValue(t);
    }
}
