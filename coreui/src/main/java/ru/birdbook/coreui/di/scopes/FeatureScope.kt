package ru.birdbook.coreui.di.scopes

import javax.inject.Scope

@Scope
@Retention(value = AnnotationRetention.RUNTIME)
annotation class FeatureScope