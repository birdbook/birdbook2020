package ru.birdbook.coreui.dialogs

import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager

/**
 * @author Nikita Bumakov
 */
class MessageDialog : DialogFragment() {

    companion object {

        private const val KEY_MESSAGE = "MESSAGE"

        fun showWithText(fm: FragmentManager, message: String) {
            val dialog = MessageDialog()
            val args = Bundle()
            args.putString(KEY_MESSAGE, message)
            dialog.arguments = args
            dialog.show(fm, MessageDialog::class.java.name)
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val args = arguments
        val message = args!!.getString(KEY_MESSAGE, "")
        return AlertDialog.Builder(requireContext())
            .setMessage(message)
            .setPositiveButton(android.R.string.ok, null)
            .create()
    }
}
