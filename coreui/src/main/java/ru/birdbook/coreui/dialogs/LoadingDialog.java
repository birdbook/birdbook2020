package ru.birdbook.coreui.dialogs;

import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import ru.birdbook.coreui.R;

/**
 * @author Nikita Bumakov
 */
public class LoadingDialog extends AppCompatDialogFragment {

    private static final String TAG = LoadingDialog.class.getName();

    public static void setLoading(@Nullable final FragmentManager fragmentManager, @Nullable final Boolean loading) {
        if (fragmentManager == null || loading == null) {
            return;
        }
        if (loading) {
            show(fragmentManager);
        } else {
            hide(fragmentManager);
        }
    }

    public static void show(@NonNull final FragmentManager fragmentManager) {
        if (fragmentManager.findFragmentByTag(TAG) == null) {
            new LoadingDialog().showNow(fragmentManager, TAG);
        }
    }

    public static void hide(@NonNull final FragmentManager fragmentManager) {
        final Fragment dialog = fragmentManager.findFragmentByTag(TAG);
        if (dialog != null) {
            fragmentManager
                    .beginTransaction()
                    .remove(dialog)
                    .commitNowAllowingStateLoss();
        }
    }

    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCancelable(false);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable final Bundle savedInstanceState) {
        return new MaterialAlertDialogBuilder(requireActivity())
                .setView(R.layout.common_loading_dialog)
                .create();
    }
}
