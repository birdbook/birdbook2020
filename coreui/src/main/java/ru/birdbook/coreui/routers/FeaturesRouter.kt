package ru.birdbook.coreui.routers

import android.content.Context
import android.content.Intent

interface FeaturesRouter {

    fun getAuthIntent(context: Context): Intent
    fun getMainScreenIntent(context: Context): Intent
}
