package ru.birdbook.coreui.routers

import android.content.Context

interface AuthRouter {

    fun makeAuthIntent(context: Context)
}