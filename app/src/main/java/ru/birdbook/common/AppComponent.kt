package ru.birdbook.common

import android.app.Application
import android.content.res.Resources
import dagger.BindsInstance
import dagger.Component
import ru.birdbook.BirdbookApplication
import ru.birdbook.coreui.routers.FeaturesRouter
import ru.birdbook.data.core.prefs.PreferenceStorage
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AppModule::class
    ]
)
interface AppComponent {

    fun application(): Application

    fun resources(): Resources

    fun featuresRouter(): FeaturesRouter

    fun preferenceStorage(): PreferenceStorage

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: BirdbookApplication): Builder

        fun build(): AppComponent
    }
}