package ru.birdbook.common

import android.app.Application
import android.content.res.Resources
import dagger.Binds
import dagger.Module
import dagger.Provides
import ru.birdbook.BirdbookApplication
import ru.birdbook.FeaturesRouterImpl
import ru.birdbook.coreui.routers.FeaturesRouter
import ru.birdbook.data.core.prefs.PreferenceStorage
import ru.birdbook.data.core.prefs.SharedPreferenceStorage
import javax.inject.Singleton

@Module(includes = [AppModule.Declarations::class])
class AppModule {

    @Module
    interface Declarations {

        @Binds
        fun bindApplication(application: BirdbookApplication): Application

        @Binds
        @Singleton
        fun bindFeaturesRouter(impl: FeaturesRouterImpl): FeaturesRouter

        @Binds
        @Singleton
        fun bindPreferenceStorage(impl: SharedPreferenceStorage): PreferenceStorage
    }

    @Provides
    fun providesResources(app: Application): Resources = app.resources
}