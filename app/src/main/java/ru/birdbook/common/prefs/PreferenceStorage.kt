package ru.birdbook.data.core.prefs

import androidx.annotation.WorkerThread

/**
 * Storage for app and user preferences.
 */
interface PreferenceStorage {
    var cookie: String

    @WorkerThread
    fun initialize()
}
