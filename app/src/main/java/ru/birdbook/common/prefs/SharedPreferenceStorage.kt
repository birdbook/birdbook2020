package ru.birdbook.data.core.prefs

import android.app.Application
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import androidx.annotation.WorkerThread
import timber.log.Timber
import javax.inject.Inject
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty


/**
 * [PreferenceStorage] impl backed by [android.content.SharedPreferences].
 */
class SharedPreferenceStorage @Inject constructor(
    private val appContext: Application
) : PreferenceStorage {
    companion object {
        const val PREFS_NAME = "birdbook"
        const val PREF_AUTH_TOKEN = "auth_token"
    }

    private val prefs: SharedPreferences by lazy {
        Timber.d("SharedPreferences inited in %s", Thread.currentThread().name)
        appContext.applicationContext.getSharedPreferences(PREFS_NAME, MODE_PRIVATE)
    }

    override var cookie by StringPreference(prefs, PREF_AUTH_TOKEN, "")

    @WorkerThread
    override fun initialize() {
        prefs
    }
}

class BooleanPreference(
    private val preferences: SharedPreferences,
    private val name: String,
    private val defaultValue: Boolean
) : ReadWriteProperty<Any, Boolean> {

    @WorkerThread
    override fun getValue(thisRef: Any, property: KProperty<*>): Boolean {
        return preferences.getBoolean(name, defaultValue)
    }

    override fun setValue(thisRef: Any, property: KProperty<*>, value: Boolean) {
        preferences.edit()
            .putBoolean(name, value)
            .apply()
    }
}

class StringPreference(
    private val preferences: SharedPreferences,
    private val name: String,
    private val defaultValue: String
) : ReadWriteProperty<Any, String> {

    @WorkerThread
    override fun getValue(thisRef: Any, property: KProperty<*>): String {
        val value: String = preferences.getString(name, defaultValue)!!
        Timber.d("getValue %s: %s", name, value)
        return value
    }

    override fun setValue(thisRef: Any, property: KProperty<*>, value: String) {
        Timber.d("setValue %s: %s", name, value)
        preferences.edit()
            .putString(name, value)
            .apply()
    }
}

class LongPreference(
    private val preferences: SharedPreferences,
    private val name: String,
    private val defaultValue: Long
) : ReadWriteProperty<Any, Long> {

    @WorkerThread
    override fun getValue(thisRef: Any, property: KProperty<*>): Long {
        val value: Long = preferences.getLong(name, defaultValue)
        Timber.d("getValue %s: %d", name, value)
        return value
    }

    override fun setValue(thisRef: Any, property: KProperty<*>, value: Long) {
        Timber.d("setValue %s: %s", name, value)
        preferences.edit()
            .putLong(name, value)
            .apply()
    }
}
