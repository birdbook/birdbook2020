package ru.birdbook

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import ru.birdbook.coreui.livedata.SingleLiveEvent
import javax.inject.Inject

class LauncherViewModel @Inject constructor(

) : ViewModel() {

    private val _showCommonError = SingleLiveEvent<Any?>()
    val showCommonError: LiveData<Any?> get() = _showCommonError

    fun kek() {
        Log.d("kek", "kek")

        _showCommonError.postCall()
    }
}