package ru.birdbook.launcher

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import ru.birdbook.BirdbookApplication
import ru.birdbook.coreui.routers.FeaturesRouter
import ru.birdbook.data.core.prefs.PreferenceStorage
import ru.birdbook.launcher.di.LauncherComponent
import javax.inject.Inject

class LauncherActivity : AppCompatActivity() {

    @Inject
    lateinit var featuresRouter: FeaturesRouter

    @Inject
    lateinit var preference: PreferenceStorage

    override fun onCreate(savedInstanceState: Bundle?) {
        val app = applicationContext as BirdbookApplication
        LauncherComponent.provideComponent(app.appComponent)
            .inject(this)

        startActivity(
            if (preference.cookie.isEmpty()) featuresRouter.getAuthIntent(this)
            else featuresRouter.getMainScreenIntent(this)
        )
        finish()

        super.onCreate(savedInstanceState)
    }
}