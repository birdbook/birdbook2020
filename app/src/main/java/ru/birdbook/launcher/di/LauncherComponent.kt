package ru.birdbook.launcher.di

import dagger.Component
import ru.birdbook.common.AppComponent
import ru.birdbook.coreui.di.scopes.FeatureScope
import ru.birdbook.launcher.LauncherActivity

@FeatureScope
@Component(
    dependencies = [
        AppComponent::class
    ],
    modules = [
    ]
)
interface LauncherComponent {

    companion object {

        fun provideComponent(
            appComponent: AppComponent
        ): LauncherComponent {
            return DaggerLauncherComponent.factory().create(appComponent)
        }
    }

    @Component.Factory
    interface Factory {
        fun create(
            appComponent: AppComponent
        ): LauncherComponent
    }

    fun inject(activity: LauncherActivity)

}