package ru.birdbook

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import ru.birdbook.coreui.routers.FeaturesRouter
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FeaturesRouterImpl @Inject constructor() :
    FeaturesRouter {

    override fun getAuthIntent(context: Context): Intent =
        Intent().setComponent(
            ComponentName(
                context.packageName,
                "ru.birdbook.auth.AuthActivity"
            )
        )

    override fun getMainScreenIntent(context: Context): Intent =
        Intent().setComponent(
            ComponentName(
                context.packageName,
                "ru.birdbook.main.MainActivity"
            )
        )
}