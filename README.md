# Birdbook
Это тестовый проект, основная идея которого решить проблему большого AppComponent из-за SubComponent'ов в Dagger 2 посредством разбиения проекта на модули и использования Component'ов вместо SubComponent для этих модулей.  
За основу взята статья Владимира Тагакова [habr](https://habr.com/ru/company/yandex/blog/419295).
Также целью проекта являлось разбиение модулей на api и implementation для прозрачности и ускорения сборки

В проекте используется api сервиса https://birdbook.ru

Стек: Dagger2 MVVM+Clean+RxJava

На данный момент код устарел и в процессе переход на современные технологии:
* RxJava -> Coroutines
* Groovy -> kts
* Dagger -> Hilt
* Views -> Compose

Также в планах добавить Ui тесты на Espresso+Barista+MockWebServer

По всем вопросам telegram: @nbumakov

# Birdbook
This is a test project, the main is to solve the problem of a large AppComponent in Dagger 2 by dividing the project into modules and using Components instead of SubComponents for these modules.
Based on the article by Vladimir Tagakov [habr] (https://habr.com/ru/company/yandex/blog/419295).
Also, the goal of the project was to split modules into API and implementation for transparency and increasing the build speed.

The project uses the API of the service https://birdbook.ru

Stack: Dagger2 MVVM + Clean + RxJava

At the moment, the code is outdated and in the process of transition to modern technologies:
* RxJava -> Coroutines
* Groovy -> kts
* Dagger -> Hilt
* Views -> Compose

I also plan to add Ui tests for Espresso + Barista + MockWebServer

For all questions telegram: @nbumakov