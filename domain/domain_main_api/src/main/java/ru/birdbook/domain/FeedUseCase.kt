package ru.birdbook.domain

import io.reactivex.Single
import ru.birdbook.domain.model.FeedItem

interface FeedUseCase {
    fun loadFeed(): Single<List<FeedItem>>
    fun reset()
    fun isEndReached(): Boolean
}