package ru.birdbook.domain.model

data class FeedItem(
    val id: String,
    val imageUrl: String,
    val imageThumbnailUrl: String,
    val nickname:String,
    val avatarUrl:String,
    val species:String,
    val desc:String,
    val timestamp:Long
)