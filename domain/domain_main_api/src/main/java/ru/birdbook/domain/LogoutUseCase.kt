package ru.birdbook.domain

interface LogoutUseCase {

    fun logout()
}