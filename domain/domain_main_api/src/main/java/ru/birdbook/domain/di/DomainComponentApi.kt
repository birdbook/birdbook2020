package ru.birdbook.domain.di

import ru.birdbook.domain.FeedUseCase
import ru.birdbook.domain.LogoutUseCase

interface DomainComponentApi {

    fun feedUseCase(): FeedUseCase

    fun logoutUseCase(): LogoutUseCase
}