package ru.birdbook.domain.auth

import io.reactivex.Completable

interface RegisterUseCase {

    fun register(email: String, pass: String, pass2: String): Completable
}