package ru.birdbook.domain.auth

import io.reactivex.Completable

interface LoginUseCase {

    fun auth(email: String, pass: String): Completable
}