package ru.birdbook.domain.auth

import io.reactivex.Completable

interface RestoreUseCase {

    fun restore(email: String): Completable
}