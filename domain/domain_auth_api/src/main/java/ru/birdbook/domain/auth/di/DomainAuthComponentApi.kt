package ru.birdbook.domain.auth.di

import ru.birdbook.domain.auth.LoginUseCase
import ru.birdbook.domain.auth.RegisterUseCase
import ru.birdbook.domain.auth.RestoreUseCase

interface DomainAuthComponentApi {

    fun loginUseCase(): LoginUseCase

    fun registerUseCase(): RegisterUseCase

    fun restoreUseCase(): RestoreUseCase
}