package ru.birdbook.domain.di

import ru.birdbook.BirdbookApplication
import ru.birdbook.data.DataInjector
import ru.birdbook.domain.main.di.DaggerDomainComponent_DomainDependenciesComponent
import ru.birdbook.domain.main.di.DomainComponent

object DomainInjector {

    fun provideDomainApi(app: BirdbookApplication): DomainComponentApi {
        val dataAuthApi = DataInjector.provideDataApi(app)
        val domainAuthDepsComponent =
            DaggerDomainComponent_DomainDependenciesComponent.factory()
                .create(dataAuthApi)
        return DomainComponent.provideComponent(app, domainAuthDepsComponent)
    }

}