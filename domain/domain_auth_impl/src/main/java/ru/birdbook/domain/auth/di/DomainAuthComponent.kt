package ru.birdbook.domain.auth.di

import dagger.Component
import ru.birdbook.BirdbookApplication
import ru.birdbook.common.AppComponent
import ru.birdbook.data.auth.api.DataAuthComponentApi

@DomainAuthScope
@Component(
    dependencies = [
        AppComponent::class,
        DomainAuthDependencies::class
    ],
    modules = [
        DomainAuthModule::class
    ]
)
interface DomainAuthComponent :
    DomainAuthComponentApi {

    companion object {

        lateinit var component: DomainAuthComponent

        fun provideComponent(
            app: BirdbookApplication,
            deps: DomainAuthDependencies
        ): DomainAuthComponent {
            if (!Companion::component.isInitialized) {
                component = DaggerDomainAuthComponent
                    .factory()
                    .create(app.appComponent, deps)
            }
            return component
        }
    }

    @Component.Factory
    interface Factory {
        fun create(
            appComponent: AppComponent,
            domainDeps: DomainAuthDependencies
        ): DomainAuthComponent
    }

    @Component(dependencies = [DataAuthComponentApi::class])
    @DomainAuthScope
    interface DomainAuthDependenciesComponent :
        DomainAuthDependencies {
        @Component.Factory
        interface Factory {
            fun create(
                dataComponentApi: DataAuthComponentApi
            ): DomainAuthDependencies
        }
    }
}