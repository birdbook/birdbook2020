package ru.birdbook.domain.auth

import io.reactivex.Completable
import ru.birdbook.data.auth.api.AuthRepository
import ru.birdbook.domain.auth.di.DomainAuthScope
import javax.inject.Inject

@DomainAuthScope
class LoginUseCaseImpl @Inject constructor(
    private val authRepository: AuthRepository
) : LoginUseCase {

    override fun auth(email: String, pass: String): Completable =
        authRepository.login(email, pass)
}