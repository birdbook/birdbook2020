package ru.birdbook.domain.auth

import io.reactivex.Completable
import ru.birdbook.data.auth.api.AuthRepository
import ru.birdbook.domain.auth.di.DomainAuthScope
import javax.inject.Inject

@DomainAuthScope
class RegisterUseCaseImpl @Inject constructor(
    private val authRepository: AuthRepository
) : RegisterUseCase {

    override fun register(email: String, pass: String, pass2: String): Completable {
        if (pass != pass2) {
            return Completable.error(DifferentPasswordsException())
        }
        return authRepository.register(email, pass)
    }
}