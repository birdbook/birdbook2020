package ru.birdbook.domain.auth.di

import dagger.Binds
import dagger.Module
import ru.birdbook.domain.auth.*

@Module
interface DomainAuthModule {

    @Binds
    @DomainAuthScope
    fun bindLoginUseCaseImpl(impl: LoginUseCaseImpl): LoginUseCase

    @Binds
    @DomainAuthScope
    fun bindRegisterUseCase(impl: RegisterUseCaseImpl): RegisterUseCase

    @Binds
    @DomainAuthScope
    fun bindRestoreUseCase(impl: RestoreUseCaseImpl): RestoreUseCase
}