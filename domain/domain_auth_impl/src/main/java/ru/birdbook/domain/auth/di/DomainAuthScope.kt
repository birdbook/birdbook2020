package ru.birdbook.domain.auth.di

import javax.inject.Scope

@Scope
@Retention(value = AnnotationRetention.RUNTIME)
annotation class DomainAuthScope