package ru.birdbook.domain.auth.di

import ru.birdbook.data.auth.api.AuthRepository


interface DomainAuthDependencies {

    fun authRepository(): AuthRepository

}