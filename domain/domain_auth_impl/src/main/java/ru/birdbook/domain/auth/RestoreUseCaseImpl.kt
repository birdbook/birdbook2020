package ru.birdbook.domain.auth

import io.reactivex.Completable
import ru.birdbook.data.auth.api.AuthRepository
import ru.birdbook.domain.auth.di.DomainAuthScope
import javax.inject.Inject

@DomainAuthScope
class RestoreUseCaseImpl @Inject constructor(
    private val authRepository: AuthRepository
) : RestoreUseCase {

    override fun restore(email: String): Completable =
        authRepository.restore(email)
}