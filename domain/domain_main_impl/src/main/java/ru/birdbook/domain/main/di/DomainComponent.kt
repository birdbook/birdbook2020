package ru.birdbook.domain.main.di

import dagger.Component
import ru.birdbook.BirdbookApplication
import ru.birdbook.common.AppComponent
import ru.birdbook.data.api.DataComponentApi
import ru.birdbook.domain.di.DomainComponentApi

@DomainScope
@Component(
    dependencies = [
        AppComponent::class,
        DomainDependencies::class
    ],
    modules = [
        DomainModule::class
    ]
)
interface DomainComponent : DomainComponentApi {

    companion object {

        lateinit var component: DomainComponent

        fun provideComponent(
            app: BirdbookApplication,
            deps: DomainDependencies
        ): DomainComponent {
            if (!Companion::component.isInitialized) {
                component = DaggerDomainComponent.factory().create(app.appComponent, deps)
            }
            return component
        }
    }

    @Component.Factory
    interface Factory {
        fun create(
            appComponent: AppComponent,
            deps: DomainDependencies
        ): DomainComponent
    }

    @Component(dependencies = [
        DataComponentApi::class
    ])
    @DomainScope
    interface DomainDependenciesComponent :
        DomainDependencies {
        @Component.Factory
        interface Factory {
            fun create(
                dataComponentApi: DataComponentApi
            ): DomainDependencies
        }
    }
}