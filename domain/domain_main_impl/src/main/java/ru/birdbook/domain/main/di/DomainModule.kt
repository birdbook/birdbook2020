package ru.birdbook.domain.main.di

import dagger.Binds
import dagger.Module
import ru.birdbook.domain.FeedUseCase
import ru.birdbook.domain.LogoutUseCase
import ru.birdbook.domain.main.FeedUseCaseImpl
import ru.birdbook.domain.main.LogoutUseCaseImpl

@Module
interface DomainModule {

    @Binds
    @DomainScope
    fun bindFeedUseCase(impl: FeedUseCaseImpl): FeedUseCase

    @Binds
    @DomainScope
    fun bindLogoutUseCase(impl: LogoutUseCaseImpl): LogoutUseCase
}