package ru.birdbook.domain.main

import ru.birdbook.data.core.prefs.PreferenceStorage
import ru.birdbook.domain.LogoutUseCase
import javax.inject.Inject

class LogoutUseCaseImpl @Inject constructor(
    private val preferenceStorage: PreferenceStorage
) : LogoutUseCase {
    override fun logout() {
        preferenceStorage.cookie = ""
    }
}