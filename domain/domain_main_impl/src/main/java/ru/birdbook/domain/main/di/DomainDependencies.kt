package ru.birdbook.domain.main.di

import ru.birdbook.data.api.MainRepository


interface DomainDependencies {

    fun mainRepository(): MainRepository

}