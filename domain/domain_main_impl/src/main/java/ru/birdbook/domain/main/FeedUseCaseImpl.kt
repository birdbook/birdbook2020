package ru.birdbook.domain.main

import io.reactivex.Single
import ru.birdbook.data.api.MainRepository
import ru.birdbook.domain.FeedUseCase
import ru.birdbook.domain.model.FeedItem
import javax.inject.Inject

class FeedUseCaseImpl @Inject constructor(
    private val repository: MainRepository,
    private val itemMapper: FeedItemMapper
) : FeedUseCase {

    private val cache = ArrayList<FeedItem>()

    private var lastT: String? = null
    private var endReached: Boolean = false


    override fun loadFeed(): Single<List<FeedItem>> = repository.loadFeed(0, lastT, 1)
        .doOnSuccess { if (it.isNotEmpty()) lastT = it.last().t }
        .map { it.map { item -> itemMapper.map(item) } }
        .doOnSuccess { it.forEach { item -> if (!cache.contains(item)) cache.add(item) } }
        .doOnSuccess { if (it.size < 10) endReached = true }
        .map { cache.sortedByDescending { it.timestamp } }

    override fun reset() {
        lastT = null
        cache.clear()
        endReached = false
    }

    override fun isEndReached() = endReached
}