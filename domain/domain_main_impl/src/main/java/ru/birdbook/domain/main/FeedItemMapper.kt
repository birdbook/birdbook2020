package ru.birdbook.domain.main

import ru.birdbook.BuildConfig
import ru.birdbook.data.api.model.FeedItemEntity
import ru.birdbook.domain.main.di.DomainScope
import ru.birdbook.domain.model.FeedItem
import javax.inject.Inject

@DomainScope
class FeedItemMapper @Inject constructor() {
    fun map(item: FeedItemEntity) =
        FeedItem(
            id = item.id.toString(),
            imageUrl = BuildConfig.BASE_URL + item.oimg,
            imageThumbnailUrl = BuildConfig.BASE_URL + item.img,
            nickname = item.pr,
            avatarUrl = item.pra ?: "",
            species = item.s,
            desc = item.desc,
            timestamp = item.t.replace(".", "")
                .substring(0, 13).toLong()
        )
}