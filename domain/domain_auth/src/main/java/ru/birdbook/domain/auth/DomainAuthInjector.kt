package ru.birdbook.domain.auth

import ru.birdbook.BirdbookApplication
import ru.birdbook.auth.DataAuthInjector
import ru.birdbook.domain.auth.di.DaggerDomainAuthComponent_DomainAuthDependenciesComponent
import ru.birdbook.domain.auth.di.DomainAuthComponent
import ru.birdbook.domain.auth.di.DomainAuthComponentApi

object DomainAuthInjector {

    fun provideAuthDomainApi(app: BirdbookApplication): DomainAuthComponentApi {
        val dataAuthApi = DataAuthInjector.provideDataAuthApi(app)
        val domainAuthDepsComponent =
            DaggerDomainAuthComponent_DomainAuthDependenciesComponent.factory()
                .create(dataAuthApi)
        return DomainAuthComponent.provideComponent(app, domainAuthDepsComponent)
    }

}