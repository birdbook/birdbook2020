package ru.birdbook.data

import ru.birdbook.BirdbookApplication
import ru.birdbook.data.api.DataComponentApi

import ru.birdbook.data.core.di.CoreDataApi
import ru.birdbook.data.core.di.CoreDataComponent
import ru.birdbook.data.di.DaggerDataComponent_DataDependenciesComponent
import ru.birdbook.data.di.DataComponent

object DataInjector {

    fun provideDataApi(app: BirdbookApplication): DataComponentApi {
        val coreDataApi: CoreDataApi = CoreDataComponent.provideCoreDataApi(app.appComponent)
        val deps =
            DaggerDataComponent_DataDependenciesComponent.factory().create(coreDataApi)
        return DataComponent.provideComponent(app, deps)
    }
}