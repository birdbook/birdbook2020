package ru.birdbook.data.api

import io.reactivex.Single
import ru.birdbook.data.api.model.FeedItemEntity

interface MainRepository {

    fun loadFeed(width: Int, t: String?, collection: Int): Single<List<FeedItemEntity>>
}