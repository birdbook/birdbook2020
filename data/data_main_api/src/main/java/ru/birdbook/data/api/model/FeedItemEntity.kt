package ru.birdbook.data.api.model

import com.google.gson.annotations.SerializedName
import java.util.*

data class FeedItemEntity(
    @SerializedName("id")
    val id: Int,
    @SerializedName("desc")
    val desc: String,
    @SerializedName("pr")
    val pr: String,
    @SerializedName("o")
    val o: String,
    @SerializedName("s")
    val s: String,
    @SerializedName("qc")
    val qc: Int,
    @SerializedName("qv")
    val qv: Int,
    @SerializedName("ql")
    val ql: Int,
    @SerializedName("cs")
    val cs: List<Objects>,
    @SerializedName("t")
    val t: String,
    @SerializedName("cls")
    val cls: List<Int>,
    @SerializedName("img")
    val img: String,
    @SerializedName("oimg")
    val oimg: String,
    @SerializedName("pra")
    val pra: String?,
    @SerializedName("q")
    val q: Int,
    @SerializedName("h")
    val h: String
)