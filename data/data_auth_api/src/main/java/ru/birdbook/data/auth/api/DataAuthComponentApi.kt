package ru.birdbook.data.auth.api

interface DataAuthComponentApi {

    fun authRepository(): AuthRepository
}