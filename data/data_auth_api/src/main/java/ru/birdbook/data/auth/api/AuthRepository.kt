package ru.birdbook.data.auth.api

import io.reactivex.Completable

interface AuthRepository {
    fun login(email: String, pass: String): Completable
    fun register(email: String, pass: String): Completable
    fun restore(email: String): Completable
}