package ru.birdbook.data.di

import dagger.Binds
import dagger.Module
import ru.birdbook.data.MainRepositoryImpl
import ru.birdbook.data.api.MainRepository
import ru.birdbook.data.core.di.DataScope

@Module
interface DataModule {

    @Binds
    @DataScope
    fun bindMainRepository(impl: MainRepositoryImpl): MainRepository

}