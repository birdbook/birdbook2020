package ru.birdbook.data

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query
import ru.birdbook.data.api.model.FeedItemEntity

interface BirdbookApiService {

    @GET("/api/profile/feed")
    fun feed(@Query("t") t: String?, @Query("collection") page: Int): Single<List<FeedItemEntity>>

}