package ru.birdbook.data.di

import dagger.Component
import ru.birdbook.BirdbookApplication
import ru.birdbook.common.AppComponent
import ru.birdbook.data.api.DataComponentApi
import ru.birdbook.data.core.di.CoreDataApi
import ru.birdbook.data.core.di.DataScope

@DataScope
@Component(
    dependencies = [
        AppComponent::class,
        DataDependencies::class
    ],
    modules = [
        DataModule::class
    ]
)
interface DataComponent : DataComponentApi {

    companion object {
        private lateinit var component: DataComponent

        fun provideComponent(
            app: BirdbookApplication,
            deps: DataDependencies
        ): DataComponentApi {
            if (!Companion::component.isInitialized) {
                component = DaggerDataComponent.factory()
                    .create(app.appComponent, deps)
            }
            return component
        }

        fun getComponent():DataComponent{
            if (!Companion::component.isInitialized) {
               throw RuntimeException()
            }
            return component
        }
    }

    @Component.Factory
    interface Factory {
        fun create(
            appComponent: AppComponent,
            deps: DataDependencies
        ): DataComponent
    }

    @DataScope
    @Component(dependencies = [
        CoreDataApi::class
    ])
    interface DataDependenciesComponent : DataDependencies {
        @Component.Factory
        interface Factory {
            fun create(
                coreDataApi: CoreDataApi
            ): DataDependencies
        }
    }
}