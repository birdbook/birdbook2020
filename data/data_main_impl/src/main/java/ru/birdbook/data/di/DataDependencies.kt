package ru.birdbook.data.di

import ru.birdbook.data.core.ServiceFactory


interface DataDependencies {

    fun serviceFactory(): ServiceFactory

}