package ru.birdbook.data

import io.reactivex.Single
import ru.birdbook.data.api.MainRepository
import ru.birdbook.data.api.model.FeedItemEntity
import ru.birdbook.data.core.ServiceFactory
import ru.birdbook.data.core.error.ErrorMapper
import timber.log.Timber
import javax.inject.Inject

class MainRepositoryImpl @Inject constructor(
    serviceFactory: ServiceFactory,
    private val errorMapper: ErrorMapper
) : MainRepository {

    init {
        Timber.d(this.javaClass.simpleName)
    }

    private val birdbookApiService: BirdbookApiService =
        serviceFactory.create(BirdbookApiService::class.java)

    override fun loadFeed(width: Int, t: String?, collection: Int): Single<List<FeedItemEntity>> =
        birdbookApiService.feed(t, collection)
            .onErrorResumeNext { errorMapper.singleError(it) }
}
