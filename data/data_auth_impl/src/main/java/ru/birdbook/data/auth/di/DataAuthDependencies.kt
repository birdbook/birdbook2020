package ru.birdbook.data.auth.di

import ru.birdbook.data.core.ServiceFactory


interface DataAuthDependencies {

    fun serviceFactory(): ServiceFactory

}