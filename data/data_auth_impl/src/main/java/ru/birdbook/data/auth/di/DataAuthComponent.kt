package ru.birdbook.data.auth.di

import dagger.Component
import ru.birdbook.BirdbookApplication
import ru.birdbook.common.AppComponent
import ru.birdbook.data.auth.api.DataAuthComponentApi
import ru.birdbook.data.core.di.CoreDataApi
import ru.birdbook.data.core.di.DataScope

@DataScope
@Component(
    dependencies = [
        AppComponent::class,
        DataAuthDependencies::class
    ],
    modules = [
        DataAuthModule::class
    ]
)
interface DataAuthComponent : DataAuthComponentApi {

    companion object {
        lateinit var component: DataAuthComponent

        fun provideComponent(
            app: BirdbookApplication,
            deps: DataAuthDependencies
        ): DataAuthComponentApi {
            if (!Companion::component.isInitialized) {
                component = DaggerDataAuthComponent.factory()
                    .create(app.appComponent, deps)
            }
            return component
        }
    }

    @Component.Factory
    interface Factory {
        fun create(
            appComponent: AppComponent,
            deps: DataAuthDependencies
        ): DataAuthComponent
    }

    @DataScope
    @Component(dependencies = [CoreDataApi::class])
    interface DataAuthDependenciesComponent : DataAuthDependencies {
        @Component.Factory
        interface Factory {
            fun create(
                coreDataApi: CoreDataApi
            ): DataAuthDependencies
        }
    }
}