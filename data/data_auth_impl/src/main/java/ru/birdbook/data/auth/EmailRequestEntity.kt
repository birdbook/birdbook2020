package ru.birdbook.data.auth

import com.google.gson.annotations.SerializedName

data class EmailRequestEntity(
    @SerializedName("e") private val email: String
)