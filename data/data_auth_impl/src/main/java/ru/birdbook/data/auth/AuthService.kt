package ru.birdbook.data.auth

import io.reactivex.Completable
import retrofit2.http.Body
import retrofit2.http.POST

interface AuthService {

    @POST("/auth/login/")
    fun login(@Body body: AuthRequestEntity): Completable

    @POST("/auth/register/")
    fun register(@Body body: AuthRequestEntity): Completable

    @POST("/auth/pr_setup/")
    fun restore(@Body body: EmailRequestEntity): Completable

}