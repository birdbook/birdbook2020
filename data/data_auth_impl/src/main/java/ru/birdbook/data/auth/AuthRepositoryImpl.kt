package ru.birdbook.data.auth

import io.reactivex.Completable
import ru.birdbook.data.auth.api.AuthRepository
import ru.birdbook.data.core.ServiceFactory
import ru.birdbook.data.core.error.ErrorMapper
import javax.inject.Inject

class AuthRepositoryImpl @Inject constructor(
    serviceFactory: ServiceFactory,
    private val errorMapper: ErrorMapper
) : AuthRepository {

    private val authService: AuthService = serviceFactory.create(AuthService::class.java)

    override fun login(email: String, pass: String): Completable =
        authService.login(AuthRequestEntity(email, pass))
            .onErrorResumeNext(errorMapper::completableError)

    override fun register(email: String, pass: String): Completable =
        authService.register(AuthRequestEntity(email, pass))
            .onErrorResumeNext(errorMapper::completableError)

    override fun restore(email: String): Completable =
        authService.restore(EmailRequestEntity(email))
            .onErrorResumeNext(errorMapper::completableError)
}
