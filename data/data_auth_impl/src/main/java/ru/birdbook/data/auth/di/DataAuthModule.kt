package ru.birdbook.data.auth.di

import dagger.Binds
import dagger.Module
import ru.birdbook.data.auth.api.AuthRepository
import ru.birdbook.data.auth.AuthRepositoryImpl
import ru.birdbook.data.core.di.DataScope

@Module
interface DataAuthModule {

    @Binds
    @DataScope
    fun bindAuthRepository(impl: AuthRepositoryImpl): AuthRepository

}