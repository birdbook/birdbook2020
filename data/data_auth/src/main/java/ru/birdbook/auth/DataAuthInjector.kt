package ru.birdbook.auth

import ru.birdbook.BirdbookApplication
import ru.birdbook.data.auth.api.DataAuthComponentApi
import ru.birdbook.data.auth.di.DaggerDataAuthComponent_DataAuthDependenciesComponent
import ru.birdbook.data.auth.di.DataAuthComponent
import ru.birdbook.data.core.di.CoreDataApi
import ru.birdbook.data.core.di.CoreDataComponent

object DataAuthInjector {

    fun provideDataAuthApi(app: BirdbookApplication): DataAuthComponentApi {
        val coreDataApi: CoreDataApi = CoreDataComponent.provideCoreDataApi(app.appComponent)
        val deps =
            DaggerDataAuthComponent_DataAuthDependenciesComponent.factory().create(coreDataApi)
        return DataAuthComponent.provideComponent(app, deps)
    }
}