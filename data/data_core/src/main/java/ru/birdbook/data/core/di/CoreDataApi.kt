package ru.birdbook.data.core.di

import ru.birdbook.data.core.ServiceFactory


interface CoreDataApi {

    fun serviceFactory(): ServiceFactory
}