package ru.birdbook.data.core.error

import io.reactivex.Completable
import io.reactivex.CompletableSource
import io.reactivex.Single
import io.reactivex.SingleSource
import ru.birdbook.data.core.di.DataScope
import javax.inject.Inject

@DataScope
class ErrorMapper @Inject constructor(
    private val requestExceptionMapper: RequestExceptionMapper
) {

    fun completableError(throwable: Throwable): CompletableSource =
        Completable.error(requestExceptionMapper.mapErrors(throwable))

    fun <T> singleError(throwable: Throwable): SingleSource<T> =
        Single.error(requestExceptionMapper.mapErrors(throwable))

}