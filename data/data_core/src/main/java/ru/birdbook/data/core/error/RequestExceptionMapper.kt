package ru.birdbook.data.core.error

import retrofit2.HttpException
import ru.birdbook.data.core.di.DataScope
import ru.greenminds.runner.common.errors.*
import java.net.ConnectException
import java.net.HttpURLConnection
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.util.*
import javax.inject.Inject

@DataScope
class RequestExceptionMapper @Inject constructor() {

    companion object {
        private val NETWORK_EXCEPTIONS = Arrays.asList<Class<*>>(
            UnknownHostException::class.java,
            SocketTimeoutException::class.java,
            ConnectException::class.java
        )
    }

    fun mapErrors(throwable: Throwable): Error =
        if (NETWORK_EXCEPTIONS.contains(throwable.javaClass)) {
            NetworkError()
        } else if (throwable is HttpException) {
            if (throwable.code() == HttpURLConnection.HTTP_UNAUTHORIZED || throwable.code() == HttpURLConnection.HTTP_FORBIDDEN) {
                UnauthorizedError()
            } else if (throwable.code() >= 500) {
                UnexpectedError(throwable)
            } else if (throwable.code() >= 400) {
                ServerError(throwable.message())
            } else {
                UnexpectedError(throwable)
            }
        } else {
            UnexpectedError(throwable)
        }
}