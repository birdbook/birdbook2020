package ru.birdbook.data.core

import okhttp3.Interceptor
import okhttp3.Response
import ru.birdbook.data.core.prefs.PreferenceStorage

class AddCookiesInterceptor(
    private val preference: PreferenceStorage
) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val builder = chain.request().newBuilder()
        builder.addHeader("Cookie", preference.cookie)
        return chain.proceed(builder.build())
    }
}