package ru.birdbook.data.core.di

import dagger.Binds
import dagger.Module
import ru.birdbook.data.core.ServiceFactory
import ru.birdbook.data.core.ServiceFactoryImpl
import ru.birdbook.data.core.prefs.PreferenceStorage
import ru.birdbook.data.core.prefs.SharedPreferenceStorage

@Module
interface CoreDataModule {

    @Binds
    @DataScope
    fun bindServiceFactory(impl: ServiceFactoryImpl): ServiceFactory

}