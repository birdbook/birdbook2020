package ru.birdbook.data.core.di

import dagger.Component
import ru.birdbook.common.AppComponent
import ru.birdbook.data.core.OkHttpModule
import ru.birdbook.data.core.RetrofitModule

@DataScope
@Component(
    dependencies = [
        AppComponent::class
    ],
    modules = [
        CoreDataModule::class,
        OkHttpModule::class,
        RetrofitModule::class
    ]
)
interface CoreDataComponent : CoreDataApi {

    companion object {
        lateinit var component: CoreDataComponent

        fun provideCoreDataApi(appComponent: AppComponent): CoreDataApi {
            if (!Companion::component.isInitialized) {
                component = DaggerCoreDataComponent
                    .builder()
                    .appComponent(appComponent)
                    .build()
            }
            return component
        }
    }
}