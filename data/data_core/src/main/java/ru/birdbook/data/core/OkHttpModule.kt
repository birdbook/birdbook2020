package ru.birdbook.data.core

import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import ru.birdbook.data.core.di.DataScope
import ru.birdbook.data.core.prefs.PreferenceStorage
import java.util.concurrent.TimeUnit
import javax.inject.Named

@Module
class OkHttpModule {

    companion object {
        private const val CONNECT_TIMEOUT = 15L
        private const val READ_WRITE_TIMEOUT = 30L
    }

    @Provides
    @DataScope
    @Named("logging-interceptor")
    fun provideLoggingInterceptor(): Interceptor {
        val interceptor = HttpLoggingInterceptor()
        if (BuildConfig.DEBUG) {
            interceptor.level = HttpLoggingInterceptor.Level.BODY
        } else {
            interceptor.level = HttpLoggingInterceptor.Level.NONE
        }
        return interceptor
    }

    @Provides
    @DataScope
    fun provideOkHttpClient(
        @Named("logging-interceptor") loggingInterceptor: Interceptor,
        preferenceStorage: PreferenceStorage
    ): OkHttpClient = OkHttpClient.Builder()
        .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
        .readTimeout(READ_WRITE_TIMEOUT, TimeUnit.SECONDS)
        .writeTimeout(READ_WRITE_TIMEOUT, TimeUnit.SECONDS)
        .addInterceptor(loggingInterceptor)
        .addInterceptor(AddCookiesInterceptor(preferenceStorage))
        .addNetworkInterceptor(ReceivedCookiesInterceptor(preferenceStorage))
        .build()
}