package ru.birdbook.data.core

import okhttp3.Interceptor
import okhttp3.Response
import ru.birdbook.data.core.prefs.PreferenceStorage
import java.util.*

class ReceivedCookiesInterceptor(
    private val preferenceStorage: PreferenceStorage
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val originalResponse = chain.proceed(chain.request())
        if (originalResponse.headers("Set-Cookie").isNotEmpty()) {
            val cookies = HashSet<String>()
            for (header in originalResponse.headers("Set-Cookie")) {
                cookies.add(header)
            }
            if (cookies.isNotEmpty()) {
                preferenceStorage.cookie = cookies.elementAt(0)
            }
        }
        return originalResponse
    }
}