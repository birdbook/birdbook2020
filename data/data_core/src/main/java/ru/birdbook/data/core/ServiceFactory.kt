package ru.birdbook.data.core

import retrofit2.Retrofit
import timber.log.Timber
import javax.inject.Inject

interface ServiceFactory {

    fun <T> create(service: Class<T>): T
}


class ServiceFactoryImpl @Inject constructor(
    private val retrofit: Retrofit
) : ServiceFactory {

    init {
        Timber.d(this.javaClass.simpleName)
    }

    override fun <T> create(service: Class<T>): T = retrofit.create(service)
}