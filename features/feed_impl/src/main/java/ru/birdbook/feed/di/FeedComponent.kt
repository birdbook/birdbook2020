package ru.birdbook.feed.di

import dagger.Component
import ru.birdbook.BirdbookApplication
import ru.birdbook.common.AppComponent
import ru.birdbook.coreui.di.ViewModelModule
import ru.birdbook.coreui.di.scopes.FeatureScope
import ru.birdbook.domain.di.DomainComponentApi
import ru.birdbook.feed.FeedFeatureApi
import ru.birdbook.feed.FeedFragment

@FeatureScope
@Component(
    dependencies = [
        AppComponent::class,
        FeedDependencies::class
    ],
    modules = [
        FeedModule::class,
        ViewModelModule::class
    ]
)
interface FeedComponent : FeedFeatureApi {

    companion object {
        private lateinit var component: FeedComponent

        fun provideComponent(
            app: BirdbookApplication,
            deps: FeedDependencies
        ): FeedFeatureApi {
            if (!Companion::component.isInitialized) {
                component = DaggerFeedComponent.factory()
                    .create(app.appComponent, deps)
            }
            return component
        }

        fun getComponent(): FeedComponent {
            if (!Companion::component.isInitialized) {
                throw RuntimeException()
            }
            return component
        }
    }

    fun inject(feedFragment: FeedFragment)

    @Component.Factory
    interface Factory {
        fun create(
            appComponent: AppComponent,
            deps: FeedDependencies
        ): FeedComponent
    }

    @FeatureScope
    @Component(dependencies = [
        DomainComponentApi::class
    ])
    interface FeedDependenciesComponent : FeedDependencies {
        @Component.Factory
        interface Factory {
            fun create(
                domainApi:DomainComponentApi
            ): FeedDependencies
        }
    }
}