package ru.birdbook.feed

import androidx.fragment.app.Fragment
import javax.inject.Inject

class FeedExternalRouterImpl @Inject constructor() : FeedExternalRouter {

    override fun createFeedFragment(): Fragment =
        FeedFragment()
}