package ru.birdbook.feed

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import ru.birdbook.coreui.error.ErrorCallbackImpl
import ru.birdbook.coreui.error.RxError
import ru.birdbook.coreui.livedata.SingleLiveEvent
import ru.birdbook.coreui.rx.RxLoadingUtils
import ru.birdbook.domain.FeedUseCase
import javax.inject.Inject


class FeedViewModel @Inject constructor(
    private val feedUseCase: FeedUseCase
) : ViewModel() {

    private val _ptrLoadingLiveData: MutableLiveData<Boolean> = MutableLiveData()
    val ptrLoadingLiveData: LiveData<Boolean> get() = _ptrLoadingLiveData

    private val _initLoadingLiveData: MutableLiveData<Boolean> = MutableLiveData()
    val initLoadingLiveData: LiveData<Boolean> get() = _initLoadingLiveData

    private val _feedLiveData: MutableLiveData<List<Any>> = MutableLiveData()
    val feedLiveData: LiveData<List<Any>> get() = _feedLiveData

    private val _showSwwErrorEvent: SingleLiveEvent<Any?> = SingleLiveEvent()
    val showSwwErrorEvent: LiveData<Any?> get() = _showSwwErrorEvent

    private val _unauthEvent: SingleLiveEvent<Any?> = SingleLiveEvent()
    val unauthEvent: LiveData<Any?> get() = _unauthEvent

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()
    private var feedDisposable: Disposable? = null

    init {
        loadFeed(_initLoadingLiveData)
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

    fun onRefresh() {
        feedUseCase.reset()
        loadFeed(_ptrLoadingLiveData)
    }

    fun onEndReached() {
        if (feedDisposable?.isDisposed == false) {
            return
        }
        if (feedUseCase.isEndReached()) {
            return
        }
        loadFeed(_ptrLoadingLiveData)
    }

    private fun loadFeed(loading: MutableLiveData<Boolean>) {
        feedDisposable?.dispose()
        feedDisposable = feedUseCase.loadFeed()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .compose(RxLoadingUtils.singleLoading(loading))
            .subscribe(_feedLiveData::postValue, RxError.error(FeedErrorCallback())::accept)
        compositeDisposable.add(feedDisposable!!)
    }

    inner class FeedErrorCallback : ErrorCallbackImpl() {

        override fun onUnauthorizedError() {
            super.onUnauthorizedError()
            _unauthEvent.postCall()
        }

        override fun onNetworkError() {
            super.onNetworkError()
            _showSwwErrorEvent.postCall()
        }

        override fun onServerError(message: String) {
            super.onServerError(message)
            _showSwwErrorEvent.postCall()
        }

        override fun onUnexpectedError(throwable: Throwable) {
            super.onUnexpectedError(throwable)
            _showSwwErrorEvent.postCall()
        }
    }
}