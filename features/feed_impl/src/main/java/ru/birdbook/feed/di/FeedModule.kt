package ru.birdbook.feed.di

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import ru.birdbook.coreui.di.ViewModelKey
import ru.birdbook.feed.FeedExternalRouter
import ru.birdbook.feed.FeedExternalRouterImpl
import ru.birdbook.feed.FeedViewModel

@Module
interface FeedModule {

    @Binds
    fun bindFeedRouter(impl: FeedExternalRouterImpl): FeedExternalRouter

    @Binds
    @IntoMap
    @ViewModelKey(FeedViewModel::class)
    fun bindFeedViewModel(viewModel: FeedViewModel): ViewModel

}