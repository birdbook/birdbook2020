package ru.birdbook.feed

import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegate
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter
import ru.birdbook.domain.model.FeedItem


object Delegates {

    var fmt: DateTimeFormatter = DateTimeFormat.forPattern("HH:mm  d MMMM, yyyy")

    fun feedItemDelegate() =
        adapterDelegate<FeedItem, Any>(R.layout.feed_item) {

            val image: ImageView = findViewById(R.id.image)
            val avatar: ImageView = findViewById(R.id.avatar)
            val nickname: TextView = findViewById(R.id.nickname)
            val species: TextView = findViewById(R.id.species)
            val time: TextView = findViewById(R.id.time)

            bind {
                image.maxHeight = image.width * 3 / 2
                image.minimumHeight = image.width

                Glide.with(image)
                    .load(item.imageUrl)
                    .thumbnail(Glide.with(image).load(item.imageThumbnailUrl))
                    .placeholder(R.drawable.feed_item_placeholder)
                    .into(image)

                Glide.with(avatar)
                    .load(item.avatarUrl)
                    .placeholder(R.drawable.ic_account_placeholder)
                    .circleCrop()
                    .into(avatar)

                nickname.text = item.nickname
                species.text = item.species
                time.text = fmt.print(item.timestamp)
            }
        }
}