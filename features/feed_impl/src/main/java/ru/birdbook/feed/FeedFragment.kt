package ru.birdbook.feed

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.hannesdorfmann.adapterdelegates4.AdapterDelegatesManager
import com.hannesdorfmann.adapterdelegates4.AsyncListDifferDelegationAdapter
import kotlinx.android.synthetic.main.feed_fragment.*
import ru.birdbook.coreui.list.EndlessRecyclerViewScrollListener
import ru.birdbook.domain.LogoutUseCase
import ru.birdbook.domain.model.FeedItem
import ru.birdbook.feed.Delegates.feedItemDelegate
import ru.birdbook.feed.di.FeedComponent
import ru.birdbook.launcher.LauncherActivity
import javax.inject.Inject


class FeedFragment : Fragment(R.layout.feed_fragment) {

    @Inject
    lateinit var provider: ViewModelProvider.Factory

    @Inject
    lateinit var logoutUseCase: LogoutUseCase

    private val viewModel by viewModels<FeedViewModel> { provider }

    private val callback: DiffUtil.ItemCallback<Any> =
        object : DiffUtil.ItemCallback<Any>() {
            override fun areItemsTheSame(oldItem: Any, newItem: Any): Boolean =
                when {
                    oldItem is FeedItem && newItem is FeedItem -> oldItem.id == newItem.id
                    else -> false
                }

            override fun areContentsTheSame(oldItem: Any, newItem: Any): Boolean = true
        }

    private val delegatesManager =
        AdapterDelegatesManager<List<Any>>()
            .addDelegate(feedItemDelegate())

    private val adapter = AsyncListDifferDelegationAdapter(
        callback, delegatesManager
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        FeedComponent.getComponent().inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        feed_recycler_view.adapter = adapter
        feed_recycler_view.addOnScrollListener(RecyclerViewScrollListener(feed_recycler_view.layoutManager as LinearLayoutManager))
        swipe_refresh.setOnRefreshListener { viewModel.onRefresh() }

        viewModel.feedLiveData.observe(viewLifecycleOwner, Observer(adapter::setItems))
        viewModel.showSwwErrorEvent.observe(
            viewLifecycleOwner,
            Observer {
                Snackbar.make(
                    view,
                    getString(ru.birdbook.coreui.R.string.sww_error_message),
                    Snackbar.LENGTH_LONG
                ).show()
            }
        )
        viewModel.unauthEvent.observe(viewLifecycleOwner, Observer { logout() })
        viewModel.ptrLoadingLiveData.observe(
            viewLifecycleOwner,
            Observer(swipe_refresh::setRefreshing)
        )
    }

    private fun logout() {
        logoutUseCase.logout()
        startActivity(Intent(requireContext(), LauncherActivity::class.java))
        requireActivity().finish()
    }

    private inner class RecyclerViewScrollListener(layoutManager: LinearLayoutManager) :
        EndlessRecyclerViewScrollListener(layoutManager) {

        override fun onEndReached() = viewModel.onEndReached()
    }
}