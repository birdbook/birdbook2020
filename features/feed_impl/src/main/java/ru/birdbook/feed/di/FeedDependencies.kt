package ru.birdbook.feed.di

import ru.birdbook.domain.FeedUseCase
import ru.birdbook.domain.LogoutUseCase

interface FeedDependencies {
    fun feedUseCase(): FeedUseCase
    fun logoutUseCase(): LogoutUseCase
}