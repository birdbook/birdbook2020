package ru.birdbook.auth

import androidx.transition.ChangeBounds
import androidx.transition.Fade
import androidx.transition.Slide
import androidx.transition.TransitionSet
import kotlinx.android.synthetic.main.authorization_fragment.*
import ru.birdbook.auth.authorization.AuthorizationFragment
import ru.birdbook.auth.registration.RegistrationFragment
import ru.birdbook.auth.restore.RestoreFragment
import ru.birdbook.coreui.di.scopes.ScreenScope
import ru.birdbook.coreui.routers.FeaturesRouter
import timber.log.Timber
import javax.inject.Inject

@ScreenScope
class AuthRouter @Inject constructor(
    private val activity: AuthActivity,
    private val featuresRouter: FeaturesRouter
) {

    companion object {
        private val transition = Transition()
        private val fade = Fade()
    }

    init {
        Timber.d(this.javaClass.simpleName)
    }

    fun showAuthorizationFragment() {
        activity.supportFragmentManager
            .beginTransaction()
            .replace(R.id.container, AuthorizationFragment())
            .commit()
    }

    fun routeToMainScreen() {
        activity.startActivity(
            featuresRouter.getMainScreenIntent(activity)
        )
        activity.finish()
    }

    fun routeToRegistration(authFragment: AuthorizationFragment) {
        val regFragment = RegistrationFragment()

        regFragment.sharedElementEnterTransition = transition
        regFragment.sharedElementReturnTransition = transition
        regFragment.enterTransition = fade
        authFragment.exitTransition = fade

        activity.supportFragmentManager
            .beginTransaction()
            .addSharedElement(authFragment.til_email, "til_email")
            .addSharedElement(authFragment.til_password, "til_password")
            .addSharedElement(authFragment.submit_button, "submit_button")
            .addSharedElement(authFragment.title, "title")
            .replace(R.id.container, regFragment)
            .addToBackStack(null)
            .commit()
    }

    fun routeToRestore(authFragment: AuthorizationFragment) {
        val restoreFragment = RestoreFragment()

        restoreFragment.sharedElementEnterTransition = transition
        restoreFragment.sharedElementReturnTransition = transition
        restoreFragment.enterTransition = fade
        authFragment.exitTransition = fade

        activity.supportFragmentManager
            .beginTransaction()
            .addSharedElement(authFragment.til_email, "til_email")
            .addSharedElement(authFragment.submit_button, "submit_button")
            .addSharedElement(authFragment.title, "title")
            .replace(R.id.container, restoreFragment)
            .addToBackStack(null)
            .commit()
    }

    class Transition : TransitionSet() {
        init {
            ordering = ORDERING_TOGETHER;
            addTransition(ChangeBounds())
            addTransition(Slide())
        }
    }
}