package ru.birdbook.auth.authorization

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Action
import io.reactivex.schedulers.Schedulers
import ru.birdbook.coreui.error.ErrorCallbackImpl
import ru.birdbook.coreui.error.RxError
import ru.birdbook.coreui.livedata.SingleLiveEvent
import ru.birdbook.coreui.rx.RxLoadingUtils
import ru.birdbook.domain.auth.LoginUseCase
import javax.inject.Inject

class AuthorizationViewModel @Inject constructor(
    private val loginUseCase: LoginUseCase
) : ViewModel() {

    private val _loadingLiveData: MutableLiveData<Boolean> = MutableLiveData()
    val loadingLiveData: LiveData<Boolean> get() = _loadingLiveData

    private val _loginSuccessEvent: SingleLiveEvent<Void> = SingleLiveEvent()
    val loginSuccessEvent: LiveData<Void> get() = _loginSuccessEvent

    private val _showWrongCredentialsEvent: SingleLiveEvent<Any?> = SingleLiveEvent()
    val showWrongCredentialsEvent: LiveData<Any?> get() = _showWrongCredentialsEvent

    private val _showSwwErrorEvent: SingleLiveEvent<Any?> = SingleLiveEvent()
    val showSwwErrorEvent: LiveData<Any?> get() = _showSwwErrorEvent

    private var loginDisposable: Disposable? = null

    override fun onCleared() {
        loginDisposable?.dispose()
        super.onCleared()
    }

    fun onSubmitClick(email: String, pass: String) {
        loginDisposable?.dispose()
        loginDisposable = loginUseCase.auth(email, pass)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .compose(RxLoadingUtils.completableLoading(_loadingLiveData))
            .subscribe(Action { _loginSuccessEvent.postCall() }, RxError.error(AuthErrorCallback()))
    }

    inner class AuthErrorCallback : ErrorCallbackImpl() {

        override fun onUnauthorizedError() {
            super.onUnauthorizedError()
            _showWrongCredentialsEvent.postCall()
        }

        override fun onNetworkError() {
            super.onNetworkError()
            _showSwwErrorEvent.postCall()
        }

        override fun onServerError(message: String) {
            super.onServerError(message)
            _showSwwErrorEvent.postCall()
        }

        override fun onUnexpectedError(throwable: Throwable) {
            super.onUnexpectedError(throwable)
            _showSwwErrorEvent.postCall()
        }
    }
}