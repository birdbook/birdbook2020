package ru.birdbook.auth.di

import dagger.Component
import ru.birdbook.BirdbookApplication
import ru.birdbook.auth.AuthActivitySubcomponent
import ru.birdbook.common.AppComponent
import ru.birdbook.coreui.di.ViewModelModule
import ru.birdbook.coreui.di.scopes.FeatureScope
import ru.birdbook.domain.auth.DomainAuthInjector
import ru.birdbook.domain.auth.di.DomainAuthComponentApi

@FeatureScope
@Component(
    dependencies = [
        AppComponent::class,
        DomainAuthComponentApi::class
    ],
    modules = [
        AuthModule::class,
        ViewModelModule::class
    ]
)
interface AuthComponent {

    companion object {

        private var instance: AuthComponent? = null

        fun provideComponent(app: BirdbookApplication): AuthComponent {
            if (instance == null) {
                instance = DaggerAuthComponent.factory().create(
                    app.appComponent,
                    DomainAuthInjector.provideAuthDomainApi(app)
                )
            }
            return instance!!
        }

        fun clearComponent() {
            instance = null
        }
    }

    val authActivitySubcomponentBuilder: AuthActivitySubcomponent.Builder

    @Component.Factory
    interface Factory {

        fun create(
            appComponent: AppComponent,
            domainComponentApi: DomainAuthComponentApi
        ): AuthComponent
    }

}