package ru.birdbook.auth.restore

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.registration_fragment.*
import ru.birdbook.auth.AuthActivity
import ru.birdbook.auth.R
import ru.birdbook.coreui.dialogs.LoadingDialog
import ru.birdbook.coreui.extensions.getCallback
import javax.inject.Inject
import ru.birdbook.coreui.R as RCore

class RestoreFragment : Fragment() {

    interface Callback {
        fun onRestoreSuccess()
    }

    @Inject
    lateinit var provider: ViewModelProvider.Factory

    private val viewModel by viewModels<RestoreViewModel> { provider }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.restore_fragment, container, false)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (activity as AuthActivity).component.inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        submit_button.setOnClickListener {
            viewModel.onSubmitClick(email.text.toString())
        }
        viewModel.registrationSuccessEvent.observe(
            viewLifecycleOwner, Observer { getCallback<Callback>()?.onRestoreSuccess() })
        viewModel.loadingLiveData.observe(
            viewLifecycleOwner, Observer { LoadingDialog.setLoading(childFragmentManager, it) })
        viewModel.showSwwErrorEvent.observe(
            viewLifecycleOwner,
            Observer {
                Snackbar.make(
                    view,
                    getString(RCore.string.sww_error_message),
                    Snackbar.LENGTH_LONG
                ).show()
            }
        )
    }
}