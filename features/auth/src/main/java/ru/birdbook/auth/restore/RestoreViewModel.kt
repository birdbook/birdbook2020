package ru.birdbook.auth.restore

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Action
import io.reactivex.schedulers.Schedulers
import ru.birdbook.coreui.error.ErrorCallbackImpl
import ru.birdbook.coreui.error.RxError
import ru.birdbook.coreui.livedata.SingleLiveEvent
import ru.birdbook.coreui.rx.RxLoadingUtils
import ru.birdbook.domain.auth.RestoreUseCase
import javax.inject.Inject

class RestoreViewModel @Inject constructor(
    private val restoreUseCase: RestoreUseCase
) : ViewModel() {

    private val _loadingLiveData: MutableLiveData<Boolean> = MutableLiveData()
    val loadingLiveData: LiveData<Boolean> get() = _loadingLiveData

    private val _registrationSuccessEvent: SingleLiveEvent<Void> = SingleLiveEvent()
    val registrationSuccessEvent: LiveData<Void> get() = _registrationSuccessEvent

    private val _showSwwErrorEvent: SingleLiveEvent<Any?> = SingleLiveEvent()
    val showSwwErrorEvent: LiveData<Any?> get() = _showSwwErrorEvent

    private var registrationDisposable: Disposable? = null

    override fun onCleared() {
        registrationDisposable?.dispose()
        super.onCleared()
    }

    fun onSubmitClick(email: String) {
        registrationDisposable?.dispose()
        registrationDisposable = restoreUseCase.restore(email)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .compose(RxLoadingUtils.completableLoading(_loadingLiveData))
            .subscribe(
                Action { _registrationSuccessEvent.postCall() },
                RxError.error(RegistrationErrorCallback())
            )
    }

    inner class RegistrationErrorCallback : ErrorCallbackImpl() {

        override fun onNetworkError() {
            super.onNetworkError()
            _showSwwErrorEvent.postCall()
        }

        override fun onServerError(message: String) {
            super.onServerError(message)
            _showSwwErrorEvent.postCall()
        }

        override fun onUnexpectedError(throwable: Throwable) {
            super.onUnexpectedError(throwable)
            _showSwwErrorEvent.postCall()
        }
    }
}