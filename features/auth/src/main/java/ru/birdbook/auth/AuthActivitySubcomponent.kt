package ru.birdbook.auth

import dagger.BindsInstance
import dagger.Subcomponent
import ru.birdbook.auth.authorization.AuthorizationFragment
import ru.birdbook.auth.registration.RegistrationFragment
import ru.birdbook.auth.restore.RestoreFragment
import ru.birdbook.coreui.di.scopes.ScreenScope

@ScreenScope
@Subcomponent(modules = [AuthActivityModule::class])
interface AuthActivitySubcomponent {

    @Subcomponent.Builder
    interface Builder {
        @BindsInstance
        fun authActivity(authActivity: AuthActivity): Builder

        fun build(): AuthActivitySubcomponent
    }

    fun inject(activity: AuthActivity)

    fun inject(authFragment: AuthorizationFragment)
    fun inject(registrationFragment: RegistrationFragment)
    fun inject(restoreFragment: RestoreFragment)
}