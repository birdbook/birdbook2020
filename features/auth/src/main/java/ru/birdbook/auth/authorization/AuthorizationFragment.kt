package ru.birdbook.auth.authorization

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.authorization_fragment.*
import ru.birdbook.auth.AuthActivity
import ru.birdbook.auth.AuthRouter
import ru.birdbook.auth.BuildConfig
import ru.birdbook.auth.R
import ru.birdbook.coreui.R as RCore
import ru.birdbook.coreui.dialogs.LoadingDialog
import ru.birdbook.coreui.dialogs.MessageDialog
import ru.birdbook.coreui.extensions.getCallback
import javax.inject.Inject

class AuthorizationFragment : Fragment() {

    interface Callback {
        fun onLoginSuccess()
    }

    @Inject
    lateinit var router: AuthRouter

    @Inject
    lateinit var provider: ViewModelProvider.Factory

    private val viewModel by viewModels<AuthorizationViewModel> { provider }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (activity as AuthActivity).component.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.authorization_fragment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        submit_button.setOnClickListener {
            viewModel.onSubmitClick(
                email.text.toString(),
                password.text.toString()
            )
        }

        sign_up.setOnClickListener { router.routeToRegistration(this) }
        forget_password.setOnClickListener { router.routeToRestore(this) }

        if (BuildConfig.DEBUG) {
            email.setText("nikita.bumakov@gmail.com")
            password.setText("jypzdq3UGQHRpcM")
        }

        viewModel.loginSuccessEvent.observe(
            viewLifecycleOwner, Observer { getCallback<Callback>()?.onLoginSuccess() })
        viewModel.loadingLiveData.observe(
            viewLifecycleOwner, Observer { LoadingDialog.setLoading(childFragmentManager, it) })
        viewModel.showWrongCredentialsEvent.observe(
            viewLifecycleOwner, Observer { showWrongCredentialsError() })
        viewModel.showSwwErrorEvent.observe(
            viewLifecycleOwner, Observer { Snackbar.make(view, getString(RCore.string.sww_error_message), Snackbar.LENGTH_LONG).show() }
        )
    }

    private fun showWrongCredentialsError() {
        MessageDialog.showWithText(
            childFragmentManager,
            getString(R.string.auth_wrong_credentials)
        )
    }
}