package ru.birdbook.auth.registration

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.registration_fragment.*
import ru.birdbook.auth.AuthActivity
import ru.birdbook.auth.R
import ru.birdbook.coreui.dialogs.LoadingDialog
import ru.birdbook.coreui.dialogs.MessageDialog
import ru.birdbook.coreui.extensions.getCallback
import javax.inject.Inject
import ru.birdbook.coreui.R as RCore

class RegistrationFragment : Fragment() {

    interface Callback {
        fun onRegistrationSuccess()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.registration_fragment, container, false)

    @Inject
    lateinit var provider: ViewModelProvider.Factory

    private val viewModel by viewModels<RegistrationViewModel> { provider }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (activity as AuthActivity).component.inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        submit_button.setOnClickListener {

            viewModel.onSubmitClick(
                email.text.toString(),
                password.text.toString(),
                password2.text.toString()
            )

            viewModel.registrationSuccessEvent.observe(
                viewLifecycleOwner, Observer { getCallback<Callback>()?.onRegistrationSuccess() })
            viewModel.loadingLiveData.observe(
                viewLifecycleOwner, Observer { LoadingDialog.setLoading(childFragmentManager, it) })
            viewModel.showDifferentPassErrorEvent.observe(
                viewLifecycleOwner, Observer { showDifferentPassError() })
            viewModel.showSwwErrorEvent.observe(
                viewLifecycleOwner,
                Observer {
                    Snackbar.make(
                        view,
                        getString(RCore.string.sww_error_message),
                        Snackbar.LENGTH_LONG
                    ).show()
                }
            )
        }
    }

    private fun showDifferentPassError() {
        MessageDialog.showWithText(
            childFragmentManager,
            getString(R.string.auth_different_passwords)
        )
    }
}