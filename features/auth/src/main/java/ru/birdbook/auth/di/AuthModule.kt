package ru.birdbook.auth.di

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import ru.birdbook.auth.AuthActivitySubcomponent
import ru.birdbook.auth.authorization.AuthorizationViewModel
import ru.birdbook.auth.registration.RegistrationViewModel
import ru.birdbook.auth.restore.RestoreViewModel
import ru.birdbook.coreui.di.ViewModelKey

@Module(subcomponents = [AuthActivitySubcomponent::class])
interface AuthModule {

    @Binds
    @IntoMap
    @ViewModelKey(AuthorizationViewModel::class)
    fun bindAuthViewModel(viewModel: AuthorizationViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RegistrationViewModel::class)
    fun bindRegistrationViewModel(viewModel: RegistrationViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RestoreViewModel::class)
    fun bindRestoreViewModel(viewModel: RestoreViewModel): ViewModel
}