package ru.birdbook.auth

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import ru.birdbook.BirdbookApplication
import ru.birdbook.auth.authorization.AuthorizationFragment
import ru.birdbook.auth.di.AuthComponent
import ru.birdbook.auth.registration.RegistrationFragment
import ru.birdbook.auth.restore.RestoreFragment
import ru.birdbook.coreui.dialogs.MessageDialog
import javax.inject.Inject


class AuthActivity : AppCompatActivity(),
    AuthorizationFragment.Callback,
    RegistrationFragment.Callback,
    RestoreFragment.Callback {

    lateinit var component: AuthActivitySubcomponent

    @Inject
    lateinit var router: AuthRouter

    override fun onCreate(savedInstanceState: Bundle?) {
        component = AuthComponent.provideComponent(applicationContext as BirdbookApplication)
            .authActivitySubcomponentBuilder
            .authActivity(this)
            .build()
        component.inject(this)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.auth_activity)

        if (savedInstanceState == null) {
            router.showAuthorizationFragment()
        }
    }

    override fun onLoginSuccess() {
        AuthComponent.clearComponent()
        router.routeToMainScreen()
    }

    override fun onRegistrationSuccess() {
        MessageDialog.showWithText(
            supportFragmentManager,
            getString(R.string.auth_registration_success_message)
        )
        if (supportFragmentManager.backStackEntryCount > 0) {
            supportFragmentManager.popBackStack()
        }
    }

    override fun onRestoreSuccess() {
        MessageDialog.showWithText(
            supportFragmentManager,
            getString(R.string.auth_password_sent_message)
        )
        if (supportFragmentManager.backStackEntryCount > 0) {
            supportFragmentManager.popBackStack()
        }
    }

}