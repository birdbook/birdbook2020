package ru.birdbook.feed

import androidx.fragment.app.Fragment

interface FeedExternalRouter {
    fun createFeedFragment(): Fragment
}