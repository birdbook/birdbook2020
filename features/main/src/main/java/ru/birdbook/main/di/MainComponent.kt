package ru.birdbook.main.di

import dagger.BindsInstance
import dagger.Component
import ru.birdbook.BirdbookApplication
import ru.birdbook.common.AppComponent
import ru.birdbook.coreui.di.ViewModelModule
import ru.birdbook.coreui.di.scopes.FeatureScope
import ru.birdbook.feed.FeedFeatureApi
import ru.birdbook.feed.FeedInjector
import ru.birdbook.main.MainActivity

@FeatureScope
@Component(
    dependencies = [
        AppComponent::class,
        MainDependencies::class
    ],
    modules = [
        MainModule::class,
        ViewModelModule::class
    ]
)
interface MainComponent {

    companion object {

        fun provideComponent(
            app: BirdbookApplication,
            activity: MainActivity
        ): MainComponent {
            return DaggerMainComponent.builder()
                .appComponent(app.appComponent)
                .bindMainActivity(activity)
                .deps(
                    DaggerMainComponent_MainDependenciesComponent.factory()
                        .create(FeedInjector.provideFeedApi(app))
                )
                .build()
        }
    }

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun bindMainActivity(activity: MainActivity): Builder

        fun appComponent(appComponent: AppComponent): Builder

        fun deps(deps: MainDependencies): Builder

        fun build(): MainComponent
    }

    @FeatureScope
    @Component(
        dependencies = [
            FeedFeatureApi::class
        ]
    )
    interface MainDependenciesComponent : MainDependencies {
        @Component.Factory
        interface Factory {
            fun create(
                feedFeatureApi: FeedFeatureApi
            ): MainDependencies
        }
    }


    fun inject(activity: MainActivity)

}