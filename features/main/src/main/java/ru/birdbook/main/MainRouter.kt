package ru.birdbook.main

import androidx.fragment.app.Fragment
import ru.birdbook.coreui.StubFragment
import ru.birdbook.feed.FeedExternalRouter
import javax.inject.Inject


class MainRouter @Inject constructor(
    private val activity: MainActivity,
    private val feedExternalRouter: FeedExternalRouter
) {

    fun openFragment(screen: MainScreens) {
        MainScreens.values()
            .forEach {
                val fragment = activity.supportFragmentManager.findFragmentByTag(it.name)
                if (it == screen) {
                    if (fragment == null) {
                        addContentFragment(screen)
                    } else {
                        showContentFragment(fragment)
                    }
                } else {
                    hideContentFragment(fragment)
                }
            }
    }

    private fun addContentFragment(mainScreen: MainScreens) {
        activity.supportFragmentManager
            .beginTransaction()
            .setCustomAnimations(R.anim.fragment_fade_scale_enter, 0)
            .add(R.id.container, createFragment(mainScreen), mainScreen.name)
            .commit()
    }

    private fun showContentFragment(fragment: Fragment) {
        activity.supportFragmentManager
            .beginTransaction()
            .setCustomAnimations(R.anim.fragment_fade_scale_enter, 0)
            .attach(fragment)
            .commit()
    }

    private fun hideContentFragment(fragment: Fragment?) {
        if (fragment == null) {
            return
        }
        activity.supportFragmentManager
            .beginTransaction()
            .setCustomAnimations(0, R.anim.fragment_fade_scale_exit)
            .detach(fragment)
            .commit()
    }

    private fun createFragment(mainScreens: MainScreens): Fragment = when (mainScreens) {
        MainScreens.FEED -> feedExternalRouter.createFeedFragment()
        MainScreens.ALBUM -> StubFragment()
        MainScreens.COLLECTION -> StubFragment()
    }
}