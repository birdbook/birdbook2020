package ru.birdbook.main.di

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import ru.birdbook.coreui.di.ViewModelKey
import ru.birdbook.main.MainActivityViewModel

@Module
interface MainModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainActivityViewModel::class)
    fun bindViewModel(viewModel: MainActivityViewModel): ViewModel
}