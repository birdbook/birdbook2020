package ru.birdbook.main

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.main_activity.*
import ru.birdbook.BirdbookApplication
import ru.birdbook.main.di.MainComponent
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var provider: ViewModelProvider.Factory

    private val viewModel by viewModels<MainActivityViewModel> { provider }

    @Inject
    lateinit var router: MainRouter

    override fun onCreate(savedInstanceState: Bundle?) {
        MainComponent.provideComponent(BirdbookApplication.instance, this)
            .inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        if (savedInstanceState == null) {
            router.openFragment(MainScreens.FEED)
        }

        bottom_navigation.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.feed -> router.openFragment(MainScreens.FEED)
                R.id.album -> router.openFragment(MainScreens.ALBUM)
                R.id.collection -> router.openFragment(MainScreens.COLLECTION)
            }
            true
        }
    }
}