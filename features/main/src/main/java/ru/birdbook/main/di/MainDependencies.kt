package ru.birdbook.main.di

import ru.birdbook.feed.FeedExternalRouter

interface MainDependencies {

    fun feedExternalRouter(): FeedExternalRouter
}