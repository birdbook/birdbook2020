package ru.birdbook.feed

import ru.birdbook.BirdbookApplication
import ru.birdbook.domain.di.DomainComponentApi
import ru.birdbook.domain.di.DomainInjector
import ru.birdbook.feed.di.DaggerFeedComponent_FeedDependenciesComponent
import ru.birdbook.feed.di.FeedComponent

object FeedInjector {

    fun provideFeedApi(app: BirdbookApplication): FeedFeatureApi {
        val domain: DomainComponentApi = DomainInjector.provideDomainApi(app)
        val deps =
            DaggerFeedComponent_FeedDependenciesComponent.factory().create(domain)
        return FeedComponent.provideComponent(app, deps)
    }
}